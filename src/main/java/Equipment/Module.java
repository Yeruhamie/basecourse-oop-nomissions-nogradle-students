package Equipment;

import AerialVehicles.ENUM.ModuleStatus;

public abstract class Module {

    private final double maxRange;
    private ModuleStatus moduleStatus;

    public Module(final double maxRange, ModuleStatus moduleStatus) {
        this.maxRange = maxRange;
        this.moduleStatus = moduleStatus;
    }

    public double getMaxRange() {
        return maxRange;
    }

    public ModuleStatus getModuleStatus() {
        return moduleStatus;
    }

    public void setModuleStatus(ModuleStatus moduleStatus) {
        this.moduleStatus = moduleStatus;
    }
}
