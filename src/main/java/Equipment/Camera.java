package Equipment;

import AerialVehicles.ENUM.ModuleStatus;

public class Camera extends Module {

    private static final double maxRange = 0.3;

    public Camera() {
        super(maxRange, ModuleStatus.NOT_DISPOSABLE);
    }
}
