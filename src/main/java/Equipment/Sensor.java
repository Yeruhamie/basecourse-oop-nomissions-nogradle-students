package Equipment;

import AerialVehicles.ENUM.ModuleStatus;

public class Sensor extends Module {

    private static final double maxRange = 0.6;

    public Sensor() {
        super(maxRange, ModuleStatus.NOT_DISPOSABLE);
    }
}
