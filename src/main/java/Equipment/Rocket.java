package Equipment;

import AerialVehicles.ENUM.ModuleStatus;

public class Rocket extends Module {

    private static final double maxRange = 1.5;

    public Rocket() {
        super(maxRange, ModuleStatus.DISPOSABLE);
    }
}
