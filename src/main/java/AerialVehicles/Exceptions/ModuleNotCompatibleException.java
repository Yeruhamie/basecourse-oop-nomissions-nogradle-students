package AerialVehicles.Exceptions;

public class ModuleNotCompatibleException extends AVException {
    public ModuleNotCompatibleException() {
        super("Module not compatible.");
    }
}
