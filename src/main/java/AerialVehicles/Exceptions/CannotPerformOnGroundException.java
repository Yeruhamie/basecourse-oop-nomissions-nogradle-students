package AerialVehicles.Exceptions;

public class CannotPerformOnGroundException extends AVException {
    public CannotPerformOnGroundException() {
        super("Cannot perform on ground.");
    }
}