package AerialVehicles.Exceptions;

public class CannotPerformInMidAirException extends AVException {
    public CannotPerformInMidAirException() {
        super("Cannot perform in mid air.");
    }
}
