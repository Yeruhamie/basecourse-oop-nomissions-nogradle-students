package AerialVehicles.Exceptions;

public abstract class AVException extends Exception {
    public AVException(String message) {
        super(message);
    }
}
