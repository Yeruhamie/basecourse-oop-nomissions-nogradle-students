package AerialVehicles.Exceptions;

public class ModuleNotFoundException extends AVException {
    public ModuleNotFoundException() {
        super("Module not found.");
    }
}
