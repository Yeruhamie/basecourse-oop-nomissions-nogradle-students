package AerialVehicles.Exceptions;

public class NoModuleStationAvailableException extends AVException {
    public NoModuleStationAvailableException() {
        super("No module station available.");
    }
}
