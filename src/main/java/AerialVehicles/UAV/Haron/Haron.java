package AerialVehicles.UAV.Haron;

import AerialVehicles.UAV.UnmannedAerialVehicle;
import Entities.Coordinates;

public abstract class Haron extends UnmannedAerialVehicle {

    private static final int kmsTillFix = 15000;

    public Haron(Coordinates currentCoordinates, final String[] listOfSupportedEquipment, final int numOfStations) {
        super(currentCoordinates, kmsTillFix, listOfSupportedEquipment, numOfStations);
    }
}

