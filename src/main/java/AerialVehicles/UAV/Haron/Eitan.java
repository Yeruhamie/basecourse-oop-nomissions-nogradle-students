package AerialVehicles.UAV.Haron;

import Entities.Coordinates;
import Equipment.Rocket;
import Equipment.Sensor;

public class Eitan extends Haron {

    private static final String[] listOfSupportedEquipment = {Rocket.class.getSimpleName(), Sensor.class.getSimpleName()};
    private static final int numOfStations = 4;

    public Eitan(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}
