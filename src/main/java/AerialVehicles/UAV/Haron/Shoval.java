package AerialVehicles.UAV.Haron;

import Entities.Coordinates;
import Equipment.Camera;
import Equipment.Rocket;
import Equipment.Sensor;

public class Shoval extends Haron {

    private static final String[] listOfSupportedEquipment = {Rocket.class.getSimpleName(), Sensor.class.getSimpleName(), Camera.class.getSimpleName()};
    private static final int numOfStations = 3;

    public Shoval(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}

