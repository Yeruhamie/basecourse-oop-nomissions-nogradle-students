package AerialVehicles.UAV.Hermes;

import Entities.Coordinates;
import Equipment.Camera;
import Equipment.Sensor;

public class Zik extends Hermes{

    private static final String[] listOfSupportedEquipment = {Sensor.class.getSimpleName(), Camera.class.getSimpleName()};
    private static final int numOfStations = 1;

    public Zik(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}