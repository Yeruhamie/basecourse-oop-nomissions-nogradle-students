package AerialVehicles.UAV.Hermes;

import AerialVehicles.UAV.UnmannedAerialVehicle;
import Entities.Coordinates;

public abstract class Hermes extends UnmannedAerialVehicle {

    private static final int kmsTillFix = 10000;

    public Hermes(Coordinates currentCoordinates, final String[] listOfSupportedEquipment, final int numOfStations) {
        super(currentCoordinates, kmsTillFix, listOfSupportedEquipment, numOfStations);
    }

}

