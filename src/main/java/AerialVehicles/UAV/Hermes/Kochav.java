package AerialVehicles.UAV.Hermes;

import Entities.Coordinates;
import Equipment.Camera;
import Equipment.Rocket;
import Equipment.Sensor;

public class Kochav extends Hermes {

    private static final String[] listOfSupportedEquipment = {Rocket.class.getSimpleName(), Sensor.class.getSimpleName(), Camera.class.getSimpleName()};
    private static final int numOfStations = 5;

    public Kochav(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}
