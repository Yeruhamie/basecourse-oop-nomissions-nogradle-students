package AerialVehicles.UAV;

import AerialVehicles.AerialVehicle;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.Exceptions.CannotPerformInMidAirException;
import Entities.Coordinates;

public abstract class UnmannedAerialVehicle extends AerialVehicle {

    private static final int avgHoveringSpeed = 150;

    public UnmannedAerialVehicle(Coordinates currentCoordinates, int kmsTillFix, final String[] listOfSupportedEquipment, final int numOfStations) {
        super(currentCoordinates, kmsTillFix, listOfSupportedEquipment, numOfStations);
    }

    public void hoverOverLocation(double hours) throws CannotPerformInMidAirException {
        if(getFlightStatus() == FlightStatus.IN_FLIGHT) {
            System.out.printf("Hovering Over: %s, %s%n", this.getCurrentCoordinates().getLongitude(), this.getCurrentCoordinates().getLatitude());
            setFlightKmsFromLastFix(getFlightKmsFromLastFix() + (avgHoveringSpeed * hours));
        } else {
            System.out.println("UAV not in flight!");
            throw new CannotPerformInMidAirException();
        }
    }

}