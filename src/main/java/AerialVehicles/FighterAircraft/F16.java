package AerialVehicles.FighterAircraft;

import Entities.Coordinates;
import Equipment.Camera;
import Equipment.Rocket;

public class F16 extends FighterAircraft {

    private static final String[] listOfSupportedEquipment = {Rocket.class.getSimpleName(), Camera.class.getSimpleName()};
    private static final int numOfStations = 7;

    public F16(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}
