package AerialVehicles.FighterAircraft;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class FighterAircraft extends AerialVehicle {
    private static final int kmsTillFix = 25000;

    public FighterAircraft(Coordinates currentCoordinates, final String[] listOfSupportedModules, final int numOfStations) {
        super(currentCoordinates, kmsTillFix, listOfSupportedModules, numOfStations);
    }
}
