package AerialVehicles.FighterAircraft;

import Entities.Coordinates;
import Equipment.Rocket;
import Equipment.Sensor;

public class F15 extends FighterAircraft {

    private static final String[] listOfSupportedEquipment = {Rocket.class.getSimpleName(), Sensor.class.getSimpleName()};
    private static final int numOfStations = 10;

    public F15(Coordinates currentCoordinates) {
        super(currentCoordinates, listOfSupportedEquipment, numOfStations);
    }
}
