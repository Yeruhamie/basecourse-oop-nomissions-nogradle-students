package AerialVehicles.ENUM;

public enum FlightStatus {
    ON_GROUND,
    TAKEN_OFF,
    IN_FLIGHT
}
