package AerialVehicles.ENUM;

public enum ModuleStatus {
    DISPOSABLE,
    NOT_DISPOSABLE,
    DISPOSABLE_AND_USED
}
