package AerialVehicles;


import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.ModuleStatus;
import AerialVehicles.Exceptions.*;
import Entities.Coordinates;
import Equipment.Module;

import java.util.ArrayList;

public abstract class AerialVehicle {
    private double flightKmsFromLastFix;
    private FlightStatus flightStatus;
    private Coordinates currentCoordinates;
    private final int kmsTillFix;
    private final String[] listOfSupportedModules;
    private final ArrayList<Module> listOfInstalledModules;
    private final int numOfStations;

    public AerialVehicle(Coordinates currentCoordinates, int kmsTillFix, final String[] listOfSupportedModules, final int numOfStations) {
        this.currentCoordinates = currentCoordinates;
        this.flightKmsFromLastFix = 0;
        this.flightStatus = FlightStatus.ON_GROUND;
        this.kmsTillFix = kmsTillFix;
        this.listOfSupportedModules = listOfSupportedModules;
        this.listOfInstalledModules = new ArrayList<Module>();
        this.numOfStations = numOfStations;
    }

    public int getNumOfStations() {
        return numOfStations;
    }

    public final String[] getListOfSupportedModules() {
        return listOfSupportedModules;
    }

    public double getFlightKmsFromLastFix() {
        return flightKmsFromLastFix;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setCurrentCoordinates(Coordinates currentCoordinates) {
        this.currentCoordinates = currentCoordinates;
    }

    public void setFlightKmsFromLastFix(double flightKmsFromLastFix) {
        this.flightKmsFromLastFix = flightKmsFromLastFix;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void takeOff() throws NeedMaintenanceException {
        if (getFlightStatus() == FlightStatus.ON_GROUND) {
            System.out.println("Taking off");
            setFlightStatus(FlightStatus.TAKEN_OFF);
        }
        else{
            System.out.println("Aerial vehicle not ready for take off!");
            throw new NeedMaintenanceException();
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformInMidAirException {
        if (getFlightStatus() == FlightStatus.TAKEN_OFF) {
            System.out.printf("Flying to %s, %s %n",destination.getLongitude(),destination.getLatitude());
            setFlightStatus(FlightStatus.IN_FLIGHT);
            setFlightKmsFromLastFix(getFlightKmsFromLastFix() + getCurrentCoordinates().distance(destination));
            setCurrentCoordinates(destination);
        }
        else{
            System.out.println("Aerial Vehicle isn't ready to fly to destination!");
            throw new CannotPerformInMidAirException();
        }
    }

    public Coordinates getCurrentCoordinates() {
        return currentCoordinates;
    }

    public String getPlatformName() {
        return getClass().getSimpleName();
    }

    public int getKmsTillFix(){
        return kmsTillFix;
    }

    public void land() throws CannotPerformInMidAirException {
        if(getFlightStatus() == FlightStatus.IN_FLIGHT) {
            System.out.println("Landing");
            setFlightStatus(FlightStatus.ON_GROUND);
        } else {
            System.out.println("Cannot land!");
            throw new CannotPerformInMidAirException();
        }
    }

    public boolean needMaintenance() {
        return getFlightKmsFromLastFix() >= getKmsTillFix();
    }

    public void performMaintenance() throws CannotPerformOnGroundException {
        if(getFlightStatus() == FlightStatus.ON_GROUND) {
            System.out.println("Performing maintenance...");
            setFlightKmsFromLastFix(0);
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    private boolean moduleIsSupported(String moduleType) {
        for(String supportedModule: getListOfSupportedModules()) {
            if(moduleType.equals(supportedModule)) {
                return true;
            }
        }

        return false;
    }

    public void loadModule(Module module) throws ModuleNotCompatibleException, NoModuleStationAvailableException {
        if (this.listOfInstalledModules.size() == getNumOfStations()) {
            System.out.println("No more available stations!");
            throw new NoModuleStationAvailableException();
        }

        if(moduleIsSupported(module.getClass().getSimpleName())) {
            this.listOfInstalledModules.add(module);
            System.out.println("The " + module.getClass().getSimpleName() + " module is successfully loaded!");
        } else {
            System.out.println("The module is not compatible!");
            throw new ModuleNotCompatibleException();
        }
    }

    private ArrayList<Module> findRequiredModules(String moduleType) {
        ArrayList<Module> installedRequiredModules = new ArrayList<Module>();
        for(Module installedModule: this.listOfInstalledModules) {
            if(moduleType.equals(installedModule.getClass().getSimpleName())) {
                installedRequiredModules.add(installedModule);
            }
        }

        return installedRequiredModules;
    }

    private boolean moduleInRange(Module module, Coordinates target) {
        return getCurrentCoordinates().distance(target) <= module.getMaxRange();
    }

    public void activateModule(String moduleType, Coordinates target) throws ModuleNotFoundException, NoModuleCanPerformException {
        ArrayList<Module> installedRequiredModules = findRequiredModules(moduleType);
        if(installedRequiredModules.isEmpty()) {
            System.out.println("The module is not installed!");
            throw new ModuleNotFoundException();
        }

        if(installedRequiredModules.get(0).getModuleStatus() != ModuleStatus.NOT_DISPOSABLE) {
            installedRequiredModules.removeIf(module -> (module.getModuleStatus() == ModuleStatus.DISPOSABLE_AND_USED));
            if(installedRequiredModules.isEmpty()) {
                System.out.println("The module required was already used!");
                throw new NoModuleCanPerformException();
            }
        }

        Module moduleToUse = installedRequiredModules.get(0);
        if(!moduleInRange(moduleToUse, target)) {
            System.out.println("The module is out of range!");
            throw new NoModuleCanPerformException();
        }

        if(moduleToUse.getModuleStatus() == ModuleStatus.DISPOSABLE) {
            moduleToUse.setModuleStatus(ModuleStatus.DISPOSABLE_AND_USED);
        }

        System.out.printf("The " + moduleToUse.getClass().getSimpleName() + " module was successfully activated on target %s, %s %n", target.getLongitude(), target.getLatitude());
    }

}

