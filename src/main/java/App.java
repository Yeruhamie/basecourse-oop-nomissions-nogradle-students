import AerialVehicles.FighterAircraft.F16;
import AerialVehicles.UAV.Haron.Eitan;
import AerialVehicles.UAV.Hermes.Kochav;
import Entities.Coordinates;
import Equipment.Camera;
import Equipment.Rocket;
import Equipment.Sensor;


public class App {


    public static void main(String[] args) {

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|Main-Driver-Start|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println();
        F16 f16 = new F16(new Coordinates(0.0, 0.0));
        Eitan eitan = new Eitan(new Coordinates(0.0, 0.0));
        Kochav kochav = new Kochav(new Coordinates(0.0, 0.0));
        try {
            f16.takeOff();
            f16.flyTo(new Coordinates(26000.0, 2.1));
            f16.land();
            if (f16.needMaintenance()) {
                System.out.println("I could see this coming...");
            }
            f16.performMaintenance();
            if (!f16.needMaintenance()) {
                System.out.println("And this too...");
            }
            f16.loadModule(new Rocket());
            f16.activateModule(Rocket.class.getSimpleName(), new Coordinates(26000.0, 2.0));

            System.out.println();

            eitan.takeOff();
            eitan.flyTo(new Coordinates(16000.0, 2.1));
            eitan.land();
            if (eitan.needMaintenance()) {
                System.out.println("I could see this coming...");
            }
            eitan.performMaintenance();
            if (!eitan.needMaintenance()) {
                System.out.println("And this too...");
            }
            eitan.loadModule(new Sensor());
            eitan.activateModule(Sensor.class.getSimpleName(), new Coordinates(16000.0, 2.0));

            System.out.println();

            kochav.takeOff();
            kochav.flyTo(new Coordinates(11000.0, 2.1));
            kochav.land();
            if (kochav.needMaintenance()) {
                System.out.println("I could see this coming...");
            }
            kochav.performMaintenance();
            if (!kochav.needMaintenance()) {
                System.out.println("And this too...");
            }
            kochav.loadModule(new Camera());
            kochav.activateModule(Camera.class.getSimpleName(), new Coordinates(11000.0, 2.0));
        } catch (Exception e) {
            System.out.println("Well, there were bugs...");
        }
    }
}
